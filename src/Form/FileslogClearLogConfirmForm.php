<?php

namespace Drupal\fileslog\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\fileslog\FilesLogManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the logs.
 */
class FileslogClearLogConfirmForm extends ConfirmFormBase {

  /**
   * The fileslog manager.
   *
   * @var \Drupal\fileslog\FilesLogManagerInterface
   */
  protected FilesLogManagerInterface $filesLogManager;

  /**
   * Constructs a new FileslogClearLogConfirmForm.
   *
   * @param \Drupal\fileslog\FilesLogManagerInterface $files_log_manager
   *   The fileslog manager.
   */
  public function __construct(FilesLogManagerInterface $files_log_manager) {
    $this->filesLogManager = $files_log_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fileslog.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fileslog_clear_log_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the recent logs?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('fileslog.overview');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->filesLogManager->deleteLogs();
    $this->messenger()->addStatus($this->t('Files log cleared.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
