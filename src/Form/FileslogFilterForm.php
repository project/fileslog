<?php

namespace Drupal\fileslog\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\fileslog\FilesLogManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the fileslog filter form.
 */
class FileslogFilterForm extends FormBase {

  const SESSION_VALUE_NAME = 'fileslog_overview_filter';

  /**
   * The fileslog manager.
   *
   * @var \Drupal\fileslog\FilesLogManagerInterface
   */
  protected FilesLogManagerInterface $filesLogManager;

  /**
   * Constructs the filter form.
   *
   * @param \Drupal\fileslog\FilesLogManagerInterface $files_log_manager
   *   The fileslog manager.
   */
  public function __construct(FilesLogManagerInterface $files_log_manager) {
    $this->filesLogManager = $files_log_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('fileslog.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fileslog_filter_form';
  }

  /**
   * Returns an array of filters.
   *
   * @return array
   *   The filters as an array.
   */
  protected function getFilters() {
    $filters = [];

    if ($channels = $this->filesLogManager->getChannels()) {
      $filters['channels'] = [
        'title' => $this->t('Type'),
        'options' => $channels,
      ];
    }

    $filters['severity'] = [
      'title' => $this->t('Severity'),
      'options' => RfcLogLevel::getLevels(),
    ];

    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $filters = $this->getFilters();
    $form['filters'] = [
      '#type' => 'details',
      '#title' => $this->t('Filter log messages'),
      '#open' => FALSE,
    ];
    $session_filters = $this->getRequest()->getSession()->get(self::SESSION_VALUE_NAME, []);

    foreach ($filters as $key => $filter) {
      $form['filters']['status'][$key] = [
        '#title' => $filter['title'],
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 8,
        '#options' => $filter['options'],
      ];

      if (!empty($session_filters[$key])) {
        $form['filters']['status'][$key]['#default_value'] = $session_filters[$key];
      }
    }

    $form['filters']['actions'] = [
      '#type' => 'actions',
      '#attributes' => ['class' => ['container-inline']],
    ];
    $form['filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    if (!empty($session_filters)) {
      $form['filters']['#open'] = TRUE;
      $form['filters']['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#limit_validation_errors' => [],
        '#submit' => ['::resetForm'],
      ];
    }

    $form['#attributes']['class'] = 'form--inline';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('channels') && $form_state->isValueEmpty('severity')) {
      $form_state->setErrorByName('channels', $this->t('You must select something to filter by.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filters = $this->getFilters();
    $session_filters = $this->getRequest()->getSession()->get(self::SESSION_VALUE_NAME, []);
    foreach ($filters as $name => $filter) {
      if ($form_state->hasValue($name)) {
        $session_filters[$name] = $form_state->getValue($name);
      }
    }
    $this->getRequest()->getSession()->set(self::SESSION_VALUE_NAME, $session_filters);
  }

  /**
   * Resets the filter form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $this->getRequest()->getSession()->remove(self::SESSION_VALUE_NAME);
  }

}
