<?php

namespace Drupal\fileslog\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\fileslog\FilesLogEntry;
use Drupal\fileslog\FilesLogManagerInterface;
use Drupal\user\UserInterface;
use Drush\Commands\DrushCommands;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\Html;
use Drush\Drupal\DrupalUtil;

/**
 * Defines the fileslog drush commands.
 */
class FileslogCommands extends DrushCommands {

  /**
   * The files log manager.
   *
   * @var \Drupal\fileslog\FilesLogManagerInterface
   */
  protected FilesLogManagerInterface $filesLogManager;

  /**
   * Constructs the fileslog commands class.
   */
  public function __construct(FilesLogManagerInterface $files_log_manager) {
    parent::__construct();

    $this->filesLogManager = $files_log_manager;
  }

  /**
   * Show fileslog messages.
   *
   * @command fileslog:show
   * @option count The number of messages to show.
   * @option extended Return extended information about each message.
   * @usage  drush fileslog:show
   *   Show a listing of most recent 10 messages.
   * @usage drush fileslog:show --count=46
   *   Show a listing of most recent 46 messages.
   * @aliases fs-show,fss,fileslog-show
   * @field-labels
   *   type: Type
   *   message: Message
   *   severity: Severity
   *   location: Location
   *   hostname: Hostname
   *   date: Date
   *   username: Username
   *   uid: Uid
   * @default-fields date,type,severity,message
   *
   * @return RowsOfFields|void
   */
  public function show($options = ['count' => 10, 'extended' => false]) {
    $table = [];
    foreach ($this->filesLogManager->getLogs([], $options['count']) as $log) {
      $table[] = (array) $this->formatResult($log, $options['extended']);
    }

    if (!empty($table)) {
      return new RowsOfFields($table);
    }

    $this->logger()->notice(dt('No log messages available.'));
  }

  /**
   * Delete fileslog log records.
   *
   * @command fileslog:delete
   * @usage drush fileslog:delete
   *   Delete all messages.
   * @aliases fs-del,fs-delete,fsd,fileslog-delete
   */
  public function delete(): void {
    $this->filesLogManager->deleteLogs();
    $this->writeln(dt('Cleared the logs.'));
  }

  /**
   * Format a watchdog database row.
   *
   * @param $result
   *   Array. A database result object.
   * @param $extended
   *   Boolean. Return extended message details.
   * @return
   *   Array. The result object with some attributes themed.
   */
  protected function formatResult(FilesLogEntry $result, $extended = false) {
    $item = new \stdClass();
    // Severity.
    $severities = RfcLogLevel::getLevels();
    $item->severity = trim(DrupalUtil::drushRender($severities[$result->severity()]));

    // Date.
    $item->date = date('d/M H:i', $result->timestamp());

    // Username.
    $item->username = (new AnonymousUserSession())->getAccountName() ?: dt('Anonymous');
    $item->username = $result->getOwner() instanceof UserInterface ? $result->getOwner()->getAccountName() : $item->username;

    // Message.
    $message_length = 188;

    // Print all the data available
    if ($extended) {
      // Possible empty values.
      if (!empty($result->link())) {
        $item->link = $result->link();
      }
      if (!empty($result->referer())) {
        $item->referer = $result->referer();
      }
      $message_length = PHP_INT_MAX;
    }
    $item->message = Unicode::truncate(strip_tags(Html::decodeEntities($result->message())), $message_length, false, false);

    return $item;
  }

}
