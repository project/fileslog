<?php

namespace Drupal\fileslog\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\fileslog\FilesLogInterface;
use Drupal\fileslog\FilesLogManagerInterface;
use Drupal\fileslog\Form\FileslogFilterForm;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Renders the fileslog routes.
 */
class FilesLogController extends ControllerBase {

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected ?Request $request;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected PagerManagerInterface $pagerManager;

  /**
   * The fileslog manager.
   *
   * @var \Drupal\fileslog\FilesLogManagerInterface
   */
  protected FilesLogManagerInterface $filesLogManager;

  /**
   * Constructs the controller.
   *
   * @param \Drupal\Core\File\FileSystemInterface $filesystem
   *   The filesystem service.
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\fileslog\FilesLogManagerInterface $files_log_manager
   *   The fileslog manager.
   */
  public function __construct(
    FileSystemInterface $filesystem,
    RequestStack $request_stack,
    FormBuilderInterface $form_builder,
    PagerManagerInterface $pager_manager,
    FilesLogManagerInterface $files_log_manager
  ) {
    $this->fileSystem = $filesystem;
    $this->request = $request_stack->getCurrentRequest();
    $this->formBuilder = $form_builder;
    $this->pagerManager = $pager_manager;
    $this->filesLogManager = $files_log_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('request_stack'),
      $container->get('form_builder'),
      $container->get('pager.manager'),
      $container->get('fileslog.manager')
    );
  }

  /**
   * View page for each log.
   *
   * @param string $filename
   *   The log's filename.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function view(string $channel, string $filename): array {
    // We use this string as an identifier for logs without a channel.
    if ($channel === 'fileslog-unknown-channel') {
      $channel = '';
    }

    if (!$log = $this->filesLogManager->getLog($channel, $filename)) {
      throw new NotFoundHttpException();
    }
    $user = $log->getOwner();

    return [
      '#type' => 'inline_template',
      '#template' => '<span class="fileslog--back-to-overview"><a href="javascript:history.back();" class="button">{{ "Back to overview"|t }}</a></span>
<table>
  <tr><th><strong>{{ "Type"|t }}</strong></th><td>{{ type }}</td></tr>
  <tr><th><strong>{{ "Date"|t }}</strong></th><td>{{ date }}</td></tr>
  <tr><th><strong>{{ "User"|t }}</strong></th><td>{{ user|raw }}</td></tr>
  <tr><th><strong>{{ "Location"|t }}</strong></th><td>{{ uri }}</td></tr>
  <tr><th><strong>{{ "Referer"|t }}</strong></th><td>{{ referer }}</td></tr>
  <tr><th><strong>{{ "Severity"|t }}</strong></th><td>{{ severity }}</td></tr>
  <tr><td colspan="2"><strong>{{ "Message"|t }}:</strong><pre>{{ message }}</pre></td></tr>
</table>',
      '#context' => [
        'type' => $log->type(),
        'severity' => $log->severity() ? $this->getLogLevelLabel($log->severity()) : '',
        'user' => $user instanceof UserInterface && $user->id() != '0'
          ? '<a href="' . $user->toUrl()->toString() . '">' . $user->label() . '</a>'
          : $this->t('Anonymous (not verified)'),
        'date' => DrupalDateTime::createFromTimestamp($log->timestamp())->format('Y-m-d H:i:s'),
        'uri' => $log->requestUri() ?? '',
        'referer' => $log->referer() ?? '',
        'message' => $log->message() ?? '',
      ],
    ];
  }

  /**
   * Overview of logs.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function overview(): array {
    $table = [
      '#type' => 'table',
      '#header' => [
        '',
        $this->t('Type'),
        $this->t('Date'),
        $this->t('Message'),
        $this->t('User'),
      ],
      '#rows' => [],
    ];

    $filters = $this->request->getSession()->get(FileslogFilterForm::SESSION_VALUE_NAME, []);
    $page = $this->pagerManager->findPage();
    $offset = $page * FilesLogInterface::ITEMS_PER_PAGE;
    $logs = $this->filesLogManager->getLogs($filters, FilesLogInterface::ITEMS_PER_PAGE, $offset);
    $total = $this->filesLogManager->getTotalLogsCount();

    foreach ($logs as $uri => $entry) {
      $message = $entry->message();
      $user = $entry->getOwner();
      $url = $entry->url() instanceof Url ? $entry->url()->toString() : '';
      if (strlen($message) > FilesLogInterface::MAX_MESSAGE_LENGTH) {
        $message = substr($message, 0, FilesLogInterface::MAX_MESSAGE_LENGTH);
        $message .= '...';
      }
      $table['#rows'][] = [
        ($entry->severity()) ? $this->getLogLevelLabel($entry->severity()) : '',
        $entry->type(),
        DrupalDateTime::createFromTimestamp($entry->timestamp())->format('Y-m-d H:i:s'),
        Markup::create('<a href="' . $url . '">' . $message . '</a>'),
        Markup::create($user instanceof UserInterface && $user->id() != 0
          ? '<a href="' . $user->toUrl()->toString() . '">' . $user->label() . '</a>'
          : $this->t('Anonymous (not verified)')),
      ];
    }

    $this->pagerManager->createPager($total, FilesLogInterface::ITEMS_PER_PAGE);

    return [
      '#type' => 'html_tag',
      '#tag' => 'div',
      'intro' => [
        '#markup' => $this->t('Showing :offset - :amount logs of total :total', [
          ':offset' => $offset + 1,
          ':amount' => min($offset + FilesLogInterface::ITEMS_PER_PAGE, $total),
          ':total' => $total,
        ]),
      ],
      'form' => $this->formBuilder()->getForm(FileslogFilterForm::class),
      'table' => $table,
      'pager' => [
        '#type' => 'pager',
        '#weight' => 10,
      ],
    ];
  }

  /**
   * Returns the log's severity as a label.
   *
   * @param int $severity
   *   The log's severity.
   *
   * @return bool|mixed
   *   The log level label or false if it doesn't exist.
   */
  protected function getLogLevelLabel(int $severity) {
    $log_levels = RfcLogLevel::getLevels();
    return $log_levels[$severity] ?? $log_levels[0] ?? FALSE;
  }

}
