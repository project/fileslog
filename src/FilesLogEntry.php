<?php

namespace Drupal\fileslog;

use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * A single fileslog entry object.
 *
 * @method int uid
 * @method int severity
 * @method int timestamp
 * @method string type
 * @method string message
 * @method string requestUri
 * @method string referer
 * @method string|null link
 * @method \Drupal\Core\Url|null url
 */
class FilesLogEntry {

  /**
   * The owner user id.
   *
   * @var int
   */
  protected int $uid;

  /**
   * The log severity.
   *
   * @var int
   */
  protected int $severity;

  /**
   * The log's timestamp.
   *
   * @var int
   */
  protected int $timestamp;

  /**
   * The log type.
   *
   * @var string
   */
  protected string $type;

  /**
   * The message.
   *
   * @var string
   */
  protected string $message;

  /**
   * The request uri.
   *
   * @var string
   */
  protected string $requestUri;

  /**
   * The referer.
   *
   * @var string
   */
  protected string $referer;

  /**
   * The link.
   *
   * @var string|null
   */
  protected ?string $link;

  /**
   * The URL.
   *
   * @var \Drupal\Core\Url|null
   */
  protected ?Url $url = NULL;

  /**
   * Creates a new entry from a values array.
   *
   * @param array $json
   *   The json array.
   *
   * @return static|bool
   */
  public static function createFromJson(array $json, ?string $channel, ?string $filename = NULL) {
    if (empty($json)) {
      return FALSE;
    }

    $instance = new static();
    $instance->uid = (int) ($json['uid'] ?? 0);
    $instance->severity = (int) ($json['severity'] ?? 0);
    $instance->timestamp = (int) ($json['timestamp'] ?? 0);
    $instance->type = $json['type'] ?? '';
    $instance->message = $json['message'] ?? '';
    $instance->requestUri = $json['request_uri'] ?? '';
    $instance->referer = $json['referer'] ?? '';
    $instance->link = $json['link'] ?? '';

    if (isset($filename)) {
      $instance->url = Url::fromRoute('fileslog.canonical', ['channel' => $channel ?? 'fileslog-unknown-channel', 'filename' => $filename]);
    }

    return $instance;
  }

  /**
   * Magic method to call properties.
   *
   * @param string $name
   *   The method or property name.
   * @param array $arguments
   *   The arguments.
   *
   * @return mixed
   */
  public function __call(string $name, array $arguments = []) {
    if (property_exists($this, $name)) {
      return $this->{$name};
    }
    else {
      throw new \BadMethodCallException('Method/property does not exist');
    }
  }

  /**
   * Returns the user entity.
   *
   * @return \Drupal\user\UserInterface|null
   */
  public function getOwner(): ?UserInterface {
    return User::load($this->uid());
  }

  /**
   * The current object as an array.
   *
   * @return array
   */
  public function __toArray(): array {
    $array = [];
    foreach (get_object_vars($this) as $property) {
      $array[$property] = $this->{$property}();
    }
    return $array;
  }

}
