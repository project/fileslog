<?php

namespace Drupal\fileslog;

/**
 * The manager for files logs.
 */
interface FilesLogManagerInterface {

  /**
   * Returns the log files.
   *
   * @param array $filters
   *   The filters to filter the results by.
   * @param int $amount
   *   The amount of items to show.
   * @param int $offset
   *   The offset to slice results with.
   *
   * @return array
   */
  public function getLogFiles(array $filters = [], int $amount = FilesLogInterface::ITEMS_PER_PAGE, int $offset = 0): array;

  /**
   * Returns the logs.
   *
   * @param array $filters
   *   The filters to filter the results by.
   * @param int $amount
   *   The amount of items to show.
   * @param int $offset
   *   The offset to slice results with.
   *
   * @return \Drupal\fileslog\FilesLogEntry[]
   */
  public function getLogs(array $filters = [], int $amount = FilesLogInterface::ITEMS_PER_PAGE, int $offset = 0): array;

  /**
   * Returns a single log.
   *
   * @param string $filename
   *   The filename.
   *
   * @return \Drupal\fileslog\FilesLogEntry|bool
   */
  public function getLog(string $channel, string $filename);

  /**
   * Adds a log.
   *
   * @param int $level
   *   The log's severity.
   * @param string $channel
   *   The log's channel.
   * @param string $entry
   *   The entry to store in the log.
   *
   * @return \Drupal\fileslog\FilesLogEntry
   *   The log object.
   */
  public function addLog(int $level, string $channel, string $entry): FilesLogEntry;

  /**
   * Returns the total logs count.
   *
   * @return int
   */
  public function getTotalLogsCount(): int;

  /**
   * Deletes all the logs.
   *
   * @return void
   */
  public function deleteLogs();

  /**
   * Retrieves the active logging channels.
   *
   * @return array
   */
  public function getChannels(): array;

}
