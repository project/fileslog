<?php

namespace Drupal\fileslog\Logger;

use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\fileslog\FilesLogInterface;

/**
 * Redirects logging messages to fileslog.
 */
class FilesLog implements FilesLogInterface {
  use RfcLoggerTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected LogMessageParserInterface $parser;

  /**
   * Constructs a fileslog object.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(LogMessageParserInterface $parser) {
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {
    global $base_url;

    // Populate the message placeholders and then replace them in the message.
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $message = empty($message_placeholders) ? $message : strtr($message, $message_placeholders);

    $channel = $context['channel'];
    $entry = json_encode([
      'base_url' => $base_url,
      'timestamp' => $context['timestamp'],
      'type' => $channel,
      'ip' => $context['ip'],
      'request_uri' => $context['request_uri'],
      'referer' => $context['referer'],
      'severity' => $level,
      'uid' => $context['uid'],
      'link' => strip_tags($context['link']),
      'message' => strip_tags($message),
    ], JSON_PRETTY_PRINT);

    /**
     * @var \Drupal\fileslog\FilesLogManagerInterface $files_log_manager
     */
    $files_log_manager = \Drupal::service('fileslog.manager');
    $files_log_manager->addLog($level, $channel, $entry);
  }

}
