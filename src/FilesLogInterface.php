<?php

namespace Drupal\fileslog;

use Psr\Log\LoggerInterface;

interface FilesLogInterface extends LoggerInterface {

  /**
   * The amount of items to show per page.
   *
   * @var int
   */
  const ITEMS_PER_PAGE = 25;

  /**
   * The directory to put logs in.
   *
   * @var string
   */
  const DIRECTORY = 'private://logs';

  /**
   * The maximum message length to show in the overview.
   *
   * @var int
   */
  const MAX_MESSAGE_LENGTH = 64;

}
