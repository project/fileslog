<?php

namespace Drupal\fileslog;

use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The manager for files logs.
 */
class FilesLogManager implements FilesLogManagerInterface {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * The total amount of logs.
   *
   * @var int
   */
  protected int $total = 0;

  /**
   * Constructs the manager.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(FileSystemInterface $file_system, RequestStack $request_stack) {
    $this->fileSystem = $file_system;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritDoc}
   */
  public function getLogFiles(array $filters = [], int $amount = FilesLogInterface::ITEMS_PER_PAGE, int $offset = 0): array {
    // Retrieve all .json files from the directory.
    $directory = FilesLogInterface::DIRECTORY;
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $files = $this->fileSystem->scanDirectory($directory, '/(.?)\.json/');
    // Sort the log files in reverse, so the latest show up first.
    uasort($files, function ($a, $b) {
      return $b->name <=> $a->name;
    });

    foreach ($files as $uri => &$file) {
      $uri = str_replace('private://logs/', '', $uri);
      $channel = NULL;
      if (str_contains($uri, '/')) {
        [$channel, $filename] = explode('/', $uri);
      }
      [$filename, $level] = explode('-', $file->name);
      $file->channel = $channel;
      $file->level = $level;
    }

    $channels = $filters['channels'] ?? [];
    if (count($channels) > 0) {
      $files = array_filter($files, function ($file) use ($channels) {
        return !empty($file->channel) && in_array($file->channel, $channels);
      });
    }

    $levels = $filters['severity'] ?? [];
    if (count($levels) > 0) {
      $files = array_filter($files, function ($file) use ($levels) {
        return in_array($file->level, $levels);
      });
    }

    $this->total = count($files);

    if ($amount === 0) {
      return $files;
    }
    return array_slice($files, $offset, $amount);
  }

  /**
   * {@inheritDoc}
   */
  public function getLogs(array $filters = [], int $amount = FilesLogInterface::ITEMS_PER_PAGE, int $offset = 0): array {
    $files = $this->getLogFiles($filters, $amount, $offset);
    $logs = [];

    foreach ($files as $uri => $file) {
      $file_contents = file_get_contents($this->fileSystem->realpath($uri));
      $log = Json::decode($file_contents);
      $logs[$uri] = FilesLogEntry::createFromJson($log, $file->channel, $file->name);
    }

    return $logs;
  }

  /**
   * {@inheritDoc}
   */
  public function getLog(string $channel, string $filename) {
    $directory = FilesLogInterface::DIRECTORY;
    if (!empty($channel)) {
      $directory .= '/' . $channel;
    }
    $uri = $directory . '/' . $filename . '.json';

    $log = [];
    if ($file_contents = @file_get_contents($this->fileSystem->realpath($uri))) {
      $log = Json::decode($file_contents);
    }

    return FilesLogEntry::createFromJson($log, $channel, $filename);
  }

  /**
   * {@inheritDoc}
   */
  public function addLog(int $level, string $channel, string $entry): FilesLogEntry {
    $directory = FilesLogInterface::DIRECTORY;
    $filename = microtime(TRUE) . '-' . $level;
    if (!empty($channel)) {
      $channel = static::toMachineName($channel);
      $directory .= '/' . $channel;
    }
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $this->fileSystem->saveData($entry, $directory . '/' . $filename . '.json');

    return $this->getlog($channel, $filename);
  }

  /**
   * {@inheritDoc}
   */
  public function getTotalLogsCount(): int {
    return $this->total;
  }

  /**
   * {@inheritDoc}
   */
  public function deleteLogs() {
    // First we need to clear out all the files.
    foreach ($this->getLogFiles([], 0) as $uri => $file) {
      $this->fileSystem->unlink($uri);
    }

    // Now we have to remove the channels.
    foreach ($this->getChannels() as $channel) {
      $this->fileSystem->rmdir(FilesLogInterface::DIRECTORY . '/' . $channel);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getChannels(): array {
    $path = $this->fileSystem->realpath(FilesLogInterface::DIRECTORY);
    $channels = [];
    $directory = dir($path);
    while ($channel = $directory->read()) {
      if ($channel !== '.' && $channel !== '..') {
        if (is_dir($path . '/' . $channel)) {
          $channels[] = $channel;
        }
      }
    }

    return array_combine($channels, $channels);
  }

  /**
   * Turns any string into a viable machine name.
   *
   * @param string $string
   *   The string to transform.
   *
   * @return string
   */
  protected static function toMachineName(string $string) {
    $transliterated = \Drupal::transliteration()->transliterate($string, LanguageInterface::LANGCODE_DEFAULT, '_');
    $transliterated = mb_strtolower($transliterated);
    return preg_replace('@[^a-z0-9_.]+@', '_', $transliterated);

  }

}
