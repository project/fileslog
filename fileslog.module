<?php

/**
 * @file
 * Redirects logging messages to the private filesystem.
 */

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\fileslog\FilesLogInterface;

/**
 * Implements hook_help().
 */
function fileslog_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.fileslog':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The fileslog module saves logs to the private filesystem.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_cron().
 */
function fileslog_cron() {
  /**
   * @var \Drupal\Core\File\FileSystemInterface $filesystem
   */
  $filesystem = \Drupal::service('file_system');
  /**
   * @var \Drupal\fileslog\FilesLogManagerInterface $files_log_manager
   */
  $files_log_manager = \Drupal::service('fileslog.manager');
  $dir = FilesLogInterface::DIRECTORY;
  $max_items = (int) \Drupal::config('fileslog.settings')->get('max_items') ?: 1000;

  $filesystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
  $files = $files_log_manager->getLogFiles([], 0);
  if (count($files) > $max_items) {
    $to_delete = array_slice($files, $max_items, (count($files) - $max_items));
    foreach ($to_delete as $uri => $file) {
      $filesystem->unlink($uri);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function fileslog_form_system_logging_settings_alter(&$form, FormStateInterface $form_state) {
  $config = \Drupal::configFactory()->getEditable('fileslog.settings');
  $form['max_items'] = [
    '#type' => 'number',
    '#title' => t('Maximum logs'),
    '#default_value' => $config->get('max_items'),
    '#required' => TRUE,
    '#description' => t('The maximum log items to keep in the private filesystem before deleting the oldest ones.'),
  ];

  $form['#submit'][] = 'fileslog_logging_settings_submit';
}

/**
 * Form submission handler for system_logging_settings().
 *
 * @see fileslog_form_system_logging_settings_alter()
 */
function fileslog_logging_settings_submit($form, FormStateInterface $form_state) {
  \Drupal::configFactory()->getEditable('fileslog.settings')
    ->set('max_items', $form_state->getValue('max_items'))
    ->save();
}
