# Fileslog

This module provides an alternative to dblog and syslog modules.
If your site is database heavy and you'd like to take some load off of your
database, but can't use syslog or want to avoid using it, this module saves
logs to the private filesystem within Drupal.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/fileslog).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/fileslog).


## Requirements

This module requires no modules outside of Drupal core, but is not designed to work in tandem with dblog.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The maximum amount of logs can be configured in the "Development > Logging and errors" section.


## Maintainers

- [Randal Vanheede (RandalV)](https://www.drupal.org/u/randalv)
- [Sven Decabooter (svendecabooter)](https://www.drupal.org/u/svendecabooter)

**Supporting organizations**

- [EntityOne](https://www.drupal.org/entityone) - Initial development &
  maintenance.
- [Make It Fly](https://www.drupal.org/make-it-fly) - Initial development &
  maintenance.
